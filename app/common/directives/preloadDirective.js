(function() {
    'use strict';

    angular.module('app.directives.preload', ['app'])

    .directive('preloadImage', preloadImage);

    function preloadImage() {
        return {
            restrict: 'A',

            link: function(scope, element) {
                element.on('load', function() {
                    //shows image after loaded
                    element.removeClass('invisible');
                });
                scope.$watch('ngSrc', function() {
                    //hides image
                    element.addClass('invisible');
                });
            }
        };
    }

})();

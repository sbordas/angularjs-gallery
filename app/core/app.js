(function() {
    'use strict';

    angular.module('app', [
        'ui.router',
        'app.gallery',
        'app.filters',
        'app.directives.preload'
    ]);
})();

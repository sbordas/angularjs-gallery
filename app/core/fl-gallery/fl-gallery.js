(function() {
    'use strict';

    angular.module('app.gallery', ['app.galleryService', 'app.modalService'])
        .component('flGallery', {
            transclude: true,
            controllerAs: 'GC',
            controller: GalleryController,
            templateUrl: 'core/fl-gallery/fl-gallery.html'
        });

    GalleryController.$inject = ['galleryService'];

    function GalleryController(galleryService) {
        var vm = this;

        this.data = 'hola';

        searchPhotos('mountains');

        function searchPhotos(searchInfo) {
            return galleryService.searchPhotos(searchInfo)
                .then(function(data) {
                    vm.gallery = data;
                    return vm.gallery;
                });
        }

        vm.checkIfEnterKeyWasPressed = function($event) {
            var keyCode = $event.which || $event.keyCode;
            if (keyCode === 13) {
                searchPhotos(vm.search);
            }
        };


    }
})();

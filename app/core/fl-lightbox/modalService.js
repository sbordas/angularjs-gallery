(function() {
    'use strict';

    angular.module('app.modalService', [])

    .factory('modalService', modalService);

    modalService.$inject = [];

    function modalService() {
        var myModal = {}; // array of modals on the page
        var service = {};
 
        service.set = set;
        //service.remove = remove;
        service.open = open;
        service.close = close;
 
        return service;
 
        //set modal
        function set(modal) {
            myModal = modal;
        }
         
        //open modal
        function open(data) {
            myModal.open(data.photo);
        }
 
        //close modal
        function close() {
            myModal.close();
        }

    }
})();







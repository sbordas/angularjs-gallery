(function() {
    'use strict';

    angular.module('app.gallery')
        .component('flLightbox', {
            transclude: true,
            controllerAs: 'LC',
            controller: LightboxController,
            templateUrl: 'core/fl-lightbox/fl-lightbox.html'
        });

    LightboxController.$inject = ['modalService'];

    function LightboxController(modalService) {
        var vm = this;

        var modal = {
            open: open,
            close: close
        };
        modalService.set(modal);

        // open modal
        function open(data) {
            vm.showLightBox = true;
            vm.data = data === undefined ? '' : data;
        }

        // close modal
        function close() {
            vm.showLightBox = false;
        }

        vm.close = function() {
            close();
        };

    }
})();

(function() {
    'use strict';

    angular.module('app.galleryService', [])

    .factory('galleryService', galleryService);

    galleryService.$inject = ['$http', '$log', '$q', 'APP_KEY'];

    function galleryService($http, $log, $q, APP_KEY) {
        return {
            searchPhotos: searchPhotos,
            getPhotoInfo: getPhotoInfo
        };

        function searchPhotos(searchInfo) {
            let tags = searchInfo.split(',');
            return $http.get('https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=' + APP_KEY + '&format=json&nojsoncallback=1&privacy_filter=1&extras=url_c,owner_name&tags=' + tags + '&search=' + searchInfo)
                .then(getPhotosComplete)
                .catch(getPhotosFailed);

            function getPhotosComplete(response) {
                return response.data;
            }

            function getPhotosFailed(e) {
                var newMessage = 'XHR Failed for searchPhotos.';
                $log.error(newMessage);
                return $q.reject(e);
            }
        }


        function getPhotoInfo(id) {
            return $http.get('https://api.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=' + APP_KEY + '&format=json&nojsoncallback=1&photo_id=' + id)
                .then(getPhotoInfoComplete)
                .catch(getPhotoInfoFailed);

            function getPhotoInfoComplete(response) {
                return response.data;
            }

            function getPhotoInfoFailed(e) {
                var newMessage = 'XHR Failed for get Photo Info.';
                $log.error(newMessage);
                return $q.reject(e);
            }
        }
    }
})();

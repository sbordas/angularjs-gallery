(function() {
    'use strict';

    angular.module('app.gallery')
        .component('flImage', {
            transclude: true,
            bindings: {
                photo: '<'
            },
            controllerAs: 'IC',
            controller: ImageController,
            templateUrl: 'core/fl-image/fl-image.html'
        });

    ImageController.$inject = ['galleryService', 'modalService'];

    function ImageController(galleryService, modalService) {
        var vm = this;

        vm.showImageBox = function(id) {
            getPhotoInfo(id).then(function(photoInfo) {
                modalService.open(photoInfo);
            });
        };

        function getPhotoInfo(id) {
            return galleryService.getPhotoInfo(id)
                .then(function(data) {
                    return data;
                });
        }
    }
})();
